import argparse
import pickle
from sentiment.models.classifier.SVM_TFIDF import SVM_TFIDF

parser = argparse.ArgumentParser()
parser.add_argument("--test_cases", type = str, default = "test/test.txt", help = "list of text to predict sentiment")
parser.add_argument("--lang", default = "viet", help = "language of input data")
parser.add_argument("--load_dir", type = str, default = "SVM/svm_model.sav", help = "path to load model file")

opt = parser.parse_args()
print(opt)

print(f"Load model from path: {opt.load_dir}")


with open(opt.test_cases) as f:
  text_list = f.readlines()
for i in range(len(text_list)):
  text_list[i] = text_list[i].replace("\n", "")
model = pickle.load(open(opt.load_dir, 'rb'))

result = model.predict(text_list)

for i in range(len(result)):
  print("Sentence:")
  print(text_list[i])
  print("Labels:")
  print(result[i])
  print()