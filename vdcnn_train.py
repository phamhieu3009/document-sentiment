import os
import numpy as np
from sklearn.model_selection import train_test_split
from keras.utils import to_categorical
from gensim.models import KeyedVectors
from keras.callbacks import ModelCheckpoint
from keras.preprocessing.sequence import pad_sequences
from sklearn.preprocessing import LabelEncoder, OneHotEncoder

from sentiment.preprocessing import eng_processing
from sentiment.embedding import get_embedding, get_embedding_matrix
from sentiment.models.classifier.VDCNN import VDCNN_model
import argparse
import pandas as pd

parser = argparse.ArgumentParser()
parser.add_argument('--trainroot', required=True, help='path to training dataset')
parser.add_argument("--valroot", type = str, default = None, help = "path to validation dataset")
parser.add_argument("--lang", default = "viet", help = "language of training data")
parser.add_argument('--train_test_split', type = int, default = 0.2,  help = "validation data size, activated if valroot is None")
parser.add_argument("--save_dir", type = str, default = "VDCNN", help = "path to save model file")
parser.add_argument("--epochs", type = int, default = 10, help = "Number of training epochs")
parser.add_argument("--batch_size", type =int, default = 16, help = "Size of training batch")


opt = parser.parse_args()
print(opt)

df = pd.read_csv(opt.trainroot)

reviews = df.review.tolist()
labels = df.label.tolist()

num_class = len(df.label.unique())

processed_review = np.array([eng_processing(review) for review in reviews])
word2vec, vector_size = get_embedding(processed_review, lang =opt.lang, processing = False)

from keras.preprocessing.text import Tokenizer
tokenizer = Tokenizer(num_words= 70000, oov_token = 0)
tokenizer.fit_on_texts(processed_review)

embedding_matrix = get_embedding_matrix(tokenizer,word2vec, vector_size, num_words= 70000)

new_processed_review = tokenizer.texts_to_sequences(processed_review)
new_processed_review = pad_sequences(new_processed_review, maxlen=100)
x_train, x_test, y_train, y_test = train_test_split(new_processed_review, labels, test_size = opt.train_test_split)

y_train = np.array(y_train)

y_test = np.array(y_test)

model = VDCNN_model(num_classes= num_class,  depth= 9, sequence_length= 100, embedding_matrix = embedding_matrix)

model.train(x_train, y_train, file_path = opt.save_dir, validation_data = (x_test, y_test), epochs = opt.epochs)
