# Document sentiment

### This module provides training and testing functionalities for three sentiment analysis models: HARNN, VDCNN, SVM_TFIDF

# A. Dependency:
 - Run "pip install -r requirements.txt"" to install Required libraries

# B. Module usage
I. To use HARNN model:
 - Run "python han_train.py --trainroot "path/to/training/data"" for training. Arguments:
 
| Arg        	   | Required      | Type  | Default            | Help 	           				                   |
|------------------|:-------------:| -----:|:------------------:|-------------------------------------------------:|
|--trainroot       | True 		   | str   |                    |path to training dataset                          |
|--valroot         | False         | str   | None               |path to validation dataset                        |
|--lang            | False         | str   | "viet"             |language of training data                         |
|--train_test_split| False         | int   | 0.2                |validation data size, activated if valroot is None|
|--save_dir		   | False         | str   | "HARNN/models"     |path to save model file                           |

 
 - Run "python han_test.py" for demo. Arguments:
 
| Arg        	   | Required      | Type  | Default               | Help 	           				                  |
|------------------|:-------------:| -----:|:---------------------:|-------------------------------------------------:|
|--test_cases      | True 		   | str   |"test/test.txt"        |list of text to predict sentiment				  |
|--lang            | False         | str   | "viet"                |language of input data                            |
|--load_dir        | False         | str   |"HARNN/models/HAN.ckpt"|path to load saved model file from                |


II. To use SVM_TFIDF model:
 - Run "python svm_train.py --trainroot "path/to/training/data"" for training. Arguments:
 
| Arg        	   | Required      | Type  | Default            | Help 	           				                   |
|------------------|:-------------:| -----:|:------------------:|-------------------------------------------------:|
|--trainroot       | True 		   | str   |                    |path to training dataset                          |
|--valroot         | False         | str   | None               |path to validation dataset                        |
|--lang            | False         | str   | "viet"             |language of training data                         |
|--train_test_split| False         | int   | 0.2                |validation data size, activated if valroot is None|
|--save_dir		   | False         | str   | "SVM"              |path to save model file                           |


 - Run "python svm_test.py" for demo. Arguments:
 
| Arg        	   | Required      | Type  | Default               | Help 	           				                  |
|------------------|:-------------:| -----:|:---------------------:|-------------------------------------------------:|
|--test_cases      | True 		   | str   |"test/test.txt"        |list of text to predict sentiment				  |
|--lang            | False         | str   | "viet"                |language of input data                            |
|--load_dir        | False         | str   |"SVM/svm_model.sav"    |path to load saved model file from                |


III. To use VDCNN model:
 - Run "python vdcnn_train.py --trainroot "path/to/training/data" for training. Arguments:
 
| Arg        	   | Required      | Type  | Default            | Help 	           				                   |
|------------------|:-------------:| -----:|:------------------:|-------------------------------------------------:|
|--trainroot       | True 		   | str   |                    |path to training dataset                          |
|--valroot         | False         | str   | None               |path to validation dataset                        |
|--lang            | False         | str   | "viet"             |language of training data                         |
|--train_test_split| False         | int   | 0.2                |validation data size, activated if valroot is None|
|--save_dir		   | False         | str   | "VDCNN"            |path to save model file                           |
|--epochs		   | False         | int   | 10                 |Number of training epochs                         |
|--batch_size	   | False         | int   | 16                 |Size of training batch                            |


 - Run "python vdcnn_test.py" for demo. Arguments:

| Arg        	   | Required      | Type  | Default                  | Help 	           				                 |
|------------------|:-------------:| -----:|:------------------------:|-------------------------------------------------:|
|--test_cases      | True 		   | str   |"test/test.txt"           |list of text to predict sentiment				 |
|--corpus          | False 		   | str   |"dataset/IMDB Dataset.csv"|path to corpus for word embedding	             |
|--lang            | False         | str   | "viet"                   |language of input data                            |
|--load_dir        | False         | str   |"VDCNN/vdcnn_weights.h5"  |path to load saved model file from                |


# C. Data preparation:

	- Training data is to be put in a ".csv" file with two columns: "review" containing sentences to train, "label" containing the label of the sentence (refer to the sample datasets in the dataset folder for more information)
	- Data to run test file is to be put in ".txt" file, with each line containing a sentence to predict label of
	
### Note: Advanced functionalities for the module (doc2vec, BERT for text classification, etc.) are currently under development
	
