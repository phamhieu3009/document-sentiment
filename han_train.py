import tensorflow as tf
from tensorflow.python.layers.base import Layer
import numpy as np
import random
import time
import matplotlib.pyplot as plt
import logging
logging.disable(logging.WARNING)
import pandas as pd
from sentiment.embedding import get_embedding
from sklearn.model_selection import train_test_split
import argparse


from sentiment.embedding import get_embedding
from sentiment.models.classifier.HARNN import HAN
from sentiment.models.classifier.HARNN import HARNN_model
from sentiment.models.classifier.HARNN import Dataset, convert_label, convert_data

parser = argparse.ArgumentParser()
parser.add_argument('--trainroot', required=True, help='path to training dataset')
parser.add_argument("--valroot", type = str, default = None, help = "path to validation dataset")
parser.add_argument("--lang", default = "viet", help = "language of training data")
parser.add_argument('--train_test_split', type = int, default = 0.2,  help = "validation data size, activated if valroot is None")
parser.add_argument("--save_dir", type = str, default = "HARNN/models", help = "path to save model file")
parser.add_argument("--epochs", type = int, default = 10, help = "Number of training epochs")
parser.add_argument("--batch_size", type =int, default = 16, help = "Size of training batch")


opt = parser.parse_args()
print(opt)


train = pd.read_csv(opt.trainroot)

train_text_list = train.review.values.tolist()
train_label = train.label.values.tolist()
num_class = len(train.label.unique())

vocab2vec, vector_size = get_embedding(train_text_list, lang = opt.lang, processing = True)


processed_labels = convert_label(train_label)

documents_train, _ , _ = convert_data(train_text_list, processed_labels)

if opt.valroot is None:
  documents_train, documents_val = train_test_split(documents_train, test_size = 0.3)
else:
  val = pd.read_csv(opt.valroot)

  val_text_list = train.review.values.tolist()
  val_label = train.label.values.tolist()

  processed_val_labels = convert_label(val_label)

  documents_val, _ , _ = convert_data(val_text_list, processed_val_labels)



han_model = HARNN_model(vocab2vec, vector_size, num_class = num_class, lang = opt.lang)

han_model.train(train_data = documents_train, validation_data = documents_val, epochs = opt.epochs, save_dir_name= opt.save_dir)
