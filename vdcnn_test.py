def warn(*args, **kwargs):
    pass
import warnings
warnings.warn = warn
import argparse
import numpy as np
import pandas as pd
from sklearn.model_selection import train_test_split

from keras.preprocessing.sequence import pad_sequences
from sentiment.embedding import get_embedding, get_embedding_matrix
from sentiment.models.classifier.VDCNN import VDCNN_model
from sentiment.preprocessing import eng_processing

parser = argparse.ArgumentParser()
parser.add_argument("--test_cases", type = str, default = "test/test.txt", help = "list of text to predict sentiment")
parser.add_argument('--corpus', type = str, default = "dataset/IMDB Dataset.csv", help='path to corpus for word embedding')
parser.add_argument('--train_test_split', type = int, default = 0.2,  help = "validation data size")
parser.add_argument("--lang", default = "viet", help = "language of input data")
parser.add_argument("--load_dir", type = str, default = "VDCNN/vdcnn_weights.h5", help = "path to load model file")

opt = parser.parse_args()
print(opt)

print(f"Loading model from path: {opt.load_dir}")
df = pd.read_csv(opt.corpus)

reviews = df.review.tolist()
labels = df.label.tolist()

num_class = len(df.label.unique())

processed_review = np.array([eng_processing(review) for review in reviews])
word2vec, vector_size = get_embedding(processed_review, lang =opt.lang, processing = False)

from keras.preprocessing.text import Tokenizer
tokenizer = Tokenizer(num_words= 70000, oov_token = 0)
tokenizer.fit_on_texts(processed_review)

embedding_matrix = get_embedding_matrix(tokenizer,word2vec, vector_size, num_words= 70000)

new_processed_review = tokenizer.texts_to_sequences(processed_review)
new_processed_review = pad_sequences(new_processed_review, maxlen=100)
x_train, x_test, y_train, y_test = train_test_split(new_processed_review, labels, test_size = opt.train_test_split)

y_train = np.array(y_train)

y_test = np.array(y_test)


model = VDCNN_model(num_classes= num_class,  depth= 9, sequence_length= 100, embedding_matrix = embedding_matrix)
model.load_weights(opt.load_dir)


with open(opt.test_cases) as f:
  text_list = f.readlines()
for i in range(len(text_list)):
  text_list[i] = text_list[i].replace("\n", "")

text = tokenizer.texts_to_sequences(text_list)
text = pad_sequences(text, maxlen=100)
result = model.predict(text)

for i in range(len(result)):
  print("Sentence:")
  print(text_list[i])
  print("Labels:")
  print(result[i])
  print()