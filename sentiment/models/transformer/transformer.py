import torch
import transformers
from transformers import XLNetModel, XLNetTokenizer, BertModel, BertTokenizer
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler
# TensorDataset để convert dữ liệu về dạng tensor, DataLoader sẽ load dữ liệu theo từng batch để bỏ vào model
# class SequentialSampler để lấy dữ liệu tuần tự, Random thì lấy ngấu nhiên
from keras.preprocessing.sequence import pad_sequences
import pandas as pd
import numpy as np


def tokenize_inputs(text_list, num_embeddings = 512, name = "xlnet"):
    """
    Tokenizes the input text input into ids. Appends the appropriate special
    characters to the end of the text to denote end of sentence. Truncate or pad
    the appropriate sequence length.
    """
    # tokenize the text, then truncate sequence to the desired length minus 2 for
    # the 2 special characters
    if name == "xlnet":
      tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased', do_lower_case=True)
    elif name == "bert":
      tokenizer = BertTokenizer.from_pretrained('bert-base-cased', do_lower_case=True)
    tokenized_texts = list(map(lambda t: tokenizer.tokenize(t)[:num_embeddings-2], text_list))
    # convert tokenized text into numeric ids for the appropriate LM
    input_ids = [tokenizer.convert_tokens_to_ids(x) for x in tokenized_texts]
    # append special token "<s>" and </s> to end of sentence
    input_ids = [tokenizer.build_inputs_with_special_tokens(x) for x in input_ids]
    # pad sequences
    input_ids = pad_sequences(input_ids, maxlen=num_embeddings, dtype="long", truncating="post", padding="post")
    return input_ids

# Class XLNetVectorization dùng để chuyển đổi doc to vec, 
# đầu vào là một list văn bản, đầu ra là một tensor chứa các document vector (768 chiều)

class XLNetVectorization(torch.nn.Module):
  
  def __init__(self):
    super(XLNetVectorization, self).__init__()
    self.xlnet = XLNetModel.from_pretrained('xlnet-base-cased')
    # self.tokenizer = XLNetTokenizer.from_pretrained('xlnet-base-cased', do_lower_case=True)

    # self.num_embeddings = num_embeddings

  def forward(self, input, token_type_ids=None,\
              attention_mask=None):
    # last hidden layer
    
    input_ids = tokenize_input(input)
    if type(input_ids) != torch.tensor:
      input_ids = torch.tensor(input_ids)

    last_hidden_state = self.xlnet(input_ids=input_ids,\
                                   attention_mask=attention_mask,\
                                   token_type_ids=token_type_ids)
    
    # pool the outputs into a mean vector
    mean_last_hidden_state = torch.mean(last_hidden_state[0], 1)
    return mean_last_hidden_state

class BertVectorization(torch.nn.Module):
  
  def __init__(self):
    super(BertVectorization, self).__init__()
    self.model = BertModel.from_pretrained('bert-base-multilingual-uncased')

  def forward(self, input, token_type_ids=None,\
              attention_mask=None):
    # last hidden layer
    
    input_ids = tokenize_input(input)
    if type(input_ids) != torch.tensor:
      input_ids = torch.tensor(input_ids)

    last_hidden_state = self.model(input_ids=input_ids,\
                                   attention_mask=attention_mask,\
                                   token_type_ids=token_type_ids)
    
    # pool the outputs into a mean vector
    mean_last_hidden_state = torch.mean(last_hidden_state[0], 1)
    return mean_last_hidden_state

# Hàm này có vai trò là nhặt từng batch trong object TensorDataset bỏ vào model transformer để xử lý
# device khuyên sử dụng là cuda vì model rất nặng, chạy bằng cpu với dữ liệu lớn (> 1000 bản ghi) rất dễ crash


def infer_in_batch(model, input_data,device="cpu", batch_size = 2, display_process =1000, save_file = False, save_name = None):
  i = 0
  vectors = []
  data = TensorDataset(input_data)
  sampler = SequentialSampler(data)
  dataloader = DataLoader(data, sampler = sampler, batch_size = batch_size)
  with torch.no_grad():
    for step, batch in enumerate(dataloader):
            # Add batch to GPU
        batch = tuple(t.to(device) for t in batch)
        # Unpack the inputs from our dataloader
        b_input_ids = batch[0]
        model.zero_grad()
        batch_vectors = transformer(b_input_ids)
        vectors.append(batch_vectors)
        i += 1
        if i % display_process == 0:
          print("Number of iterations: {}/{}".format(i, len(input_data)/batch_size))
          print(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())))
  new_vectors = []
  for vector in vectors:
    for i in range(vector.size(0)):
        a = np.array(vector[i].cpu())
        new_vectors.append(a)
  if save_file == True:
      np.save(save_name, new_vectors)
  return new_vectors