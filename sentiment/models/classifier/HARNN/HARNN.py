from .Dataset_util import Dataset
from .HAN import HAN
import tensorflow as tf
from sentiment.preprocessing.text_processing import hier_att_processing
import numpy as np
import time
# TO DO

import os


class HARNN_model():
    def __init__(self, word2vec_model, word2vec_size, num_class, lang ="viet"):
        super(HARNN_model, self).__init__()
        self.num_class = num_class
        self.word2vec_model = word2vec_model
        self.word2vec_size = word2vec_size
        self.model = HAN(self.num_class, self.word2vec_size, lamb=0.0, disTill=2.0)
        self.lang = lang
            
    def train(self, train_data, validation_data, save_dir_name, save_file = "HAN.ckpt", num_class = 2, epochs =1, print_every=50, batch_size = 16):
        

        with tf.variable_scope("train_test",reuse=tf.AUTO_REUSE):
            X_sentence = tf.placeholder(dtype=tf.float32,shape=[None,None,None,self.word2vec_size])
            y=tf.placeholder(dtype=tf.int32,shape=[None])
            sentence_length=tf.placeholder(dtype=tf.int32,shape=[None])  # it should have shape=(batch_size*document_size,)
            document_length=tf.placeholder(dtype=tf.int32,shape=[None])  # it should have shape=(batch_size,)
            is_training=tf.placeholder(dtype=tf.bool,shape=None)
            dropout=tf.placeholder(dtype=tf.float32,shape=None)
        
            loss, predict, accuracy, attention_low, attention_high = self.model(X_sentence, y, sentence_length, document_length, dropout, is_training)
        
            def eval(dataset,num_iteration, bs =16):
                tot_loss=0
                tot_accuracy=0
                dataset.start_epoch()
                for it in range(num_iteration):
                    output,document_sizes,sentence_sizes,labels=dataset.next_batch(bs)
                    feed_dict={X_sentence:output,y:labels,sentence_length:sentence_sizes.reshape(-1,),
                           document_length:document_sizes,is_training:False,dropout:0.0}
                    loss_num,acc_num=sess.run([loss,accuracy],feed_dict=feed_dict)
                    tot_loss+=loss_num
                    tot_accuracy+=acc_num
                tot_loss/=num_iteration
                tot_accuracy/=num_iteration
                return tot_loss,tot_accuracy

            global_step = tf.Variable(0, trainable=False)
            starter_learning_rate = 4e-4
            learning_rate = tf.train.exponential_decay(starter_learning_rate, global_step,len(train_data)//128, 0.9, staircase=True)
            optimizier=tf.train.AdamOptimizer(learning_rate=starter_learning_rate)
            train_step = optimizier.minimize(loss)

        
            train=Dataset(train_data, self.word2vec_model, self.word2vec_size, cut_long_sentence= 128)
            train_eval = Dataset(train_data, self.word2vec_model, self.word2vec_size, cut_long_sentence= 128)
            validation=Dataset(validation_data,self.word2vec_model, self.word2vec_size, cut_long_sentence= 128)
    
            acc_train_his=[]
            acc_val_his=[]

            loss_train_his=[]
            loss_val_his=[]
            config = tf.ConfigProto()
            config.gpu_options.allow_growth = True

    
            a = os.path.abspath(os.curdir)
            save_path = os.path.join(a, save_dir_name)
            if os.path.exists(save_path) == False:
                os.makedirs(save_path)
    
            saver=tf.train.Saver()
            with tf.Session(config=config) as sess:
                tf.global_variables_initializer().run()
                max_acc=None
                for epoch in range(epochs):
                    print(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())),
                      'start epoch %d/%d, with learning rate = %.10f' % (epoch+1,epochs,sess.run(learning_rate)))
        
                    train.start_epoch()
                    num_iteration = len(train_data)//batch_size
                    for it in range(num_iteration):
                        output,document_sizes,sentence_sizes,labels =train.next_batch(batch_size)
                        feed_dict={X_sentence:output, y:labels, sentence_length:sentence_sizes.reshape(-1,),
                               document_length:document_sizes, is_training:True,dropout:0.0}
                        loss_num,acc_num,_=sess.run([loss,accuracy,train_step],feed_dict=feed_dict)
                        if it==0 or (it+1)%print_every==0 or it==num_iteration-1:
                            print(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())),
                              'iteration %d/%d:' % (it+1,num_iteration),'current training loss = %f, accuracy = %.2f%%' % (loss_num,acc_num*100.0))
        


                    num_val = len(validation_data)
                    num_train = len(train_data)
                    loss_train,acc_train = eval(train_eval, num_train//batch_size)
                    loss_val,acc_val = eval(validation, num_val//batch_size)
                
                    acc_train_his.append(acc_train)
                    acc_val_his.append(acc_val)

                    loss_train_his.append(loss_train)
                    loss_val_his.append(loss_val)
        
                    if max_acc==None or acc_val>max_acc:
                        max_acc=acc_val
                        if save_dir_name != None:
                            save_path = saver.save(sess, save_dir_name + "/" + save_file)
                            print("Currently maximum accuracy on validation set, model saved in path: %s" % save_path)
        
                    print(time.strftime('%Y-%m-%d %H:%M:%S',time.localtime(time.time())),'end epoch %d/%d:' % (epoch+1,epochs),
                     'acc_train=%.3f%% acc_val=%.3f%%' % (acc_train*100.0,acc_val*100.0))
                    print()
    
    
    def evaluate(self, dataset, model_path = "HARNN/HAN.ckpt", bs = 16, sess = None, test_stage = True, feed_dict = None, loss_val = None, acc_val = None):

            with tf.variable_scope("train_test", reuse=tf.AUTO_REUSE):
                X_sentence = tf.placeholder(dtype=tf.float32,shape=[None,None,None,self.word2vec_size])
                y=tf.placeholder(dtype=tf.int32,shape=[None])
                sentence_length=tf.placeholder(dtype=tf.int32,shape=[None])  # it should have shape=(batch_size*document_size,)
                document_length=tf.placeholder(dtype=tf.int32,shape=[None])  # it should have shape=(batch_size,)
                is_training=tf.placeholder(dtype=tf.bool,shape=None)
                dropout=tf.placeholder(dtype=tf.float32,shape=None)
        
                loss,predict,accuracy,attention_low,attention_high= self.model(X_sentence,y,sentence_length,document_length,dropout,is_training)

                def eval(dataset,num_iteration, bs =16):
                    tot_loss=0
                    tot_accuracy=0
                    dataset.start_epoch()
                    for it in range(num_iteration):
                        output,document_sizes,sentence_sizes,labels=dataset.next_batch(bs)
                        feed_dict={X_sentence:output,y:labels,sentence_length:sentence_sizes.reshape(-1,),
                           document_length:document_sizes,is_training:False,dropout:0.0}
                        loss_num,acc_num=sess.run([loss,accuracy],feed_dict=feed_dict)
                        tot_loss+=loss_num
                        tot_accuracy+=acc_num
                    tot_loss/=num_iteration
                    tot_accuracy/=num_iteration
                    return tot_loss,tot_accuracy
            
                num_iteration = len(dataset)//bs
                data=Dataset(dataset, self.word2vec_model, self.word2vec_size, cut_long_sentence= 128)
                tot_loss=0
                tot_accuracy=0
                saver=tf.train.Saver()
            
                with tf.Session() as sess:
                    saver.restore(sess, model_path)
                    loss_val,acc_val=eval(data,num_iteration)
                    print('Accuracy is %.3f%%' % (acc_val*100.0))
                    print()
                    print('Total loss is %f' % loss_val)
        
            return tot_loss,tot_accuracy
                
    def make_input(self, doc, cut_long_sentence = 64):
        
        this_doc = hier_att_processing(doc, lang = self.lang)
        document_sizes=np.array([len(this_doc)])
        sentence_sizes=np.array([[len(t) for t in this_doc]])
        document_size=np.max(document_sizes)
        sentence_size=np.max(sentence_sizes)
        
        output=np.zeros((1,document_size,cut_long_sentence, self.word2vec_size))
        
        for (id_s,s) in enumerate(this_doc):
            for (id_w,w) in enumerate(s):
                if id_w>=cut_long_sentence:  # cut too long sentences
                    break
                elif w in self.word2vec_model:
                    output[0,id_s,id_w,:]= self.word2vec_model[w]
                else:
                    output[0,id_s,id_w,:]= np.zeros(self.word2vec_size)
        
        return this_doc, document_sizes,sentence_sizes,output
    
    def predict(self, test_cases, model_path, exact_labels = None):
        if type(test_cases) is not list:
            test = []
            test.append(test_cases)
            test_cases = test
        with tf.variable_scope("train_test",reuse=tf.AUTO_REUSE):
            X_sentence = tf.placeholder(dtype=tf.float32,shape=[None,None,None,self.word2vec_size])
            y=tf.placeholder(dtype=tf.int32,shape=[None])
            sentence_length=tf.placeholder(dtype=tf.int32,shape=[None])  # it should have shape=(batch_size*document_size,)
            document_length=tf.placeholder(dtype=tf.int32,shape=[None])  # it should have shape=(batch_size,)
            is_training=tf.placeholder(dtype=tf.bool,shape=None)
            dropout=tf.placeholder(dtype=tf.float32,shape=None)
        
        
            loss, predict, accuracy, attention_low, attention_high = self.model(X_sentence, y, sentence_length, document_length, dropout, is_training)
            saver=tf.train.Saver()
            with tf.Session() as sess:
                saver.restore(sess, model_path)
                cor=0
                for id,sent in enumerate(test_cases):
                    this_doc,document_sizes,sentence_sizes,output=self.make_input(sent)
        
                    slen=output.shape[2]
                    if exact_labels is None:
                        label = np.array([0])
                    else:
                        label=np.array([int(exact_labels[id])])
                    feed_dict={X_sentence:output,y:label,sentence_length:sentence_sizes.reshape(-1,),
                       document_length:document_sizes,is_training:False,dropout:0.0}
                    pre,al,ah=sess.run([predict,attention_low,attention_high],feed_dict=feed_dict)
                    print('Case #'+str(id+1)+':')
                    print('Original sentence:')
                    print(', '.join(sent.split('\t')))
                    print()
                    if exact_labels is None:
                        pass
                    else:
                        print('Exact label: %d' % label[0])
                    print('Predicted label: %d' % (pre[0]))
                    print()
                    print('Sentence attention:')
                    for (id_s,s) in enumerate(this_doc):
                        print('%.2f%%' % (ah[0,id_s]*100.0),' '.join(s))
                    print()
                    print('Word attention:')
                    for (id_s,s) in enumerate(this_doc):
                        print(' '.join([str(('%.2f%%' % (al[id_s,id_w]*100.0),w)) for (id_w,w) in enumerate(s)]))
                    print()
                    print()
