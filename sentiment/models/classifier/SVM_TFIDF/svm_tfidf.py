from __future__ import print_function
from sklearn import metrics
from sklearn.pipeline import Pipeline
from sklearn.feature_extraction.text import CountVectorizer,TfidfTransformer
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.svm import LinearSVC
from sklearn.linear_model import LogisticRegression
import pandas as pd
from pyvi import ViTokenizer
import re
import string
import codecs
import pickle
import os


model = LinearSVC(fit_intercept = True,multi_class='crammer_singer', C=1)
steps = []
steps.append(('CountVectorizer', CountVectorizer(ngram_range=(1,5),stop_words = None,max_df=0.5, min_df=5)))
steps.append(('tfidf', TfidfTransformer(use_idf=False, sublinear_tf = True,norm='l2',smooth_idf=True)))
steps.append(('classifier', model))
svm_model = Pipeline(steps)

class SVM_TFIDF:
    def __init__(self, svm_model = svm_model):
        self.model = svm_model    
    def train(self, X_train, y_train, X_test, y_test, save_dir = None, save_file = "svm_model.sav"):
        self.model.fit(X_train, y_train)
        print(f"Accuracy on training data: {self.model.score(X_train, y_train)}")
        print(f"Accuracy on validation data: {self.model.score(X_test, y_test)}")
        pickle.dump(self.model, open(os.path.join(save_dir, save_file), 'wb'))
        print(f"Saved model to {os.path.join(save_dir, save_file)}")
    def predict(self, X_test, use_pretrained = True, save_root = None):
        if use_pretrained:
          loaded_model = pickle.load(open(save_root, 'rb'))
          pred  = loaded_model.predict(X_test)
        else:
          try:
            pred = self.model.predict(X_test)
          except:
            raise "Model is not fitted yet, use .train() method to fit the model"
        return pred
    def evaluate(self, X, Y):
        score = self.model.score( X, Y)
        return score