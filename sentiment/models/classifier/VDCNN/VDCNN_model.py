import keras
from keras.models import Sequential, load_model, model_from_json
from keras.optimizers import SGD
from keras.callbacks import ModelCheckpoint, TensorBoard
from keras import losses
import keras.backend as K
import numpy as np
import h5py
import datetime
from keras.preprocessing.sequence import pad_sequences
from .VDCNN import VDCNN
from sklearn.preprocessing import LabelEncoder, OneHotEncoder
from sentiment.preprocessing import eng_processing, viet_processing
from keras.utils import to_categorical


# Model Hyperparameters

# Types of downsampling methods, use either three of max (maxpool), k_max (k-maxpool) or conv (linear) (default: 'max')

# Depth for VDCNN, use either 9, 17, 29 or 47 (default: 9)

# Use optional shortcut (default: False)

# Sort during k-max pooling (default: False)

# Use bias for all conv1d layers (default: False)

#Batch Size (default: 64)

#  Number of training epochs (default: 2)


class VDCNN_model:
    def __init__(self, num_classes= 2,  depth= 9, sequence_length= 100, embedding_matrix = None):
        super(VDCNN_model, self).__init__()
        self.model = VDCNN(num_classes= num_classes, depth= depth, sequence_length= sequence_length, shortcut= True, pool_type= "max", 
                           sorted= False, use_bias= True, embedding_matrix = embedding_matrix)
    
    def train(self, x_train, y_train, file_path, save_file ="vdcnn_weights.h5", batch_size= 64, epochs= 2, validation_data=None, 
            verbose=1, lr = 0.001, momentum = 0.9):
        
        self.model.compile(optimizer=SGD(lr=lr, momentum= momentum), loss='categorical_crossentropy', metrics=['accuracy'])
        checkpointer = ModelCheckpoint(filepath= file_path + "/" + save_file , period=1,
                                   verbose=1, save_best_only=True, mode='min', monitor='val_loss')
        
        
        x_test = validation_data[0]
        y_test = validation_data[1]
        
        label_encoder = LabelEncoder()
        one_hot = OneHotEncoder()
        y_train = label_encoder.fit_transform(y_train)
        y_test = label_encoder.transform(validation_data[1])
        y_train = to_categorical(y_train)
        y_test = to_categorical(y_test)
        
        
        self.model.fit(x_train, y_train, batch_size= 64, epochs= epochs, validation_data= (x_test, y_test), 
            verbose=1, callbacks=[checkpointer])
        
    def evaluate(self, x_test, y_test, batch_size = 64):

        label_encoder = LabelEncoder()

        y_test = label_encoder.fit_transform(y_test)
        y_test = to_categorical(y_test)
        score, acc = self.model.evaluate(x_test, y_test,
                            batch_size=batch_size)
    def predict(self, text_list):
        prediction = self.model.predict(text_list, batch_size=None, verbose=0)
        
        return np.argmax(prediction)
    def load_weights(self, file_path):
        self.model.load_weights(file_path)
