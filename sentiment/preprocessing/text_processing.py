from .viet_stopwords import viet_stopwords
import string
import nltk
nltk.download('stopwords')
from nltk.corpus import stopwords
eng_stopwords = set(stopwords.words('english'))
import re
import os
import pandas as pd
from pyvi.ViTokenizer import tokenize
from nltk.tokenize import TweetTokenizer
import numpy as np


def clean_text(text):
    text = re.sub('<.*?>', '', text).strip()
    text = re.sub('(\s)+', r'\1', text)
    return text
    
def sentence_segment(text):
    new_doc = []
    sents = re.split("([.?!])?[\n]+|[.?!] ", text)
    for sent in sents:
      if sent != None:
        new_doc.append(sent)
    return new_doc

    
def viet_word_segment(sent):
    sent = tokenize(sent)
    return sent

def eng_word_segment(sent):
  tknzr = TweetTokenizer()
  return tknzr.tokenize(sent)

def normalize_text(text):
    listpunctuation = string.punctuation.replace('_', '')
    for i in listpunctuation:
        text = text.replace(i, ' ')
    return text.lower()
    

def remove_viet_stopword(text):
    pre_text = []
    words = text.split()
    for word in words:
        if word not in viet_stopwords:
            pre_text.append(word)
        text = ' '.join(pre_text)
    return text

def remove_eng_stopword(text):
    pre_text = []
    words = text.split()
    for word in words:
        if word not in eng_stopwords:
            pre_text.append(word)
        text = ' '.join(pre_text)
    return text

def viet_processing(text):
    text = clean_text(text)
    text = normalize_text(text)
#    text = remove_viet_stopword(text)
    text = viet_word_segment(text)
    text = text.split()
    return text

def eng_processing(text):
    text = clean_text(text)
    text = normalize_text(text)
    text = remove_eng_stopword(text)
    text = eng_word_segment(text)
    return text

def hier_att_processing(doc, lang = "eng"):
  new_doc_hier = []
  doc = sentence_segment(doc)
  for sent in doc:
    if lang == "eng":
      sent = eng_processing(sent)
      new_doc_hier.append(sent)
    if lang == "viet":
      sent = viet_processing(sent)
      new_doc_hier.append(sent)
  return new_doc_hier