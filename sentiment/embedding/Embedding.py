import numpy as np
from ..preprocessing import viet_processing, eng_processing
from gensim.models import KeyedVectors
import os

def get_embedding(dataset, lang = "viet", processing = False):
    if processing == True:
        processed_text_list = []
        for text in dataset:
            if lang == "viet":
                normalized_text = viet_processing(text)
            elif lang == "eng":
                normalized_text = eng_processing(text)
            processed_text_list.append(normalized_text)
    else:
        processed_text_list = dataset
    vocab_set = set()
    for text in processed_text_list:
        for word in text:
            if word not in vocab_set:
                vocab_set.add(word)
    vocab2vec = dict()
    
    if lang == "viet":
        word2vec_model = KeyedVectors.load_word2vec_format("sentiment/embedding/viet_emb/baomoi_300.bin", binary=True)
    if lang == "eng":
         word2vec_model = KeyedVectors.load_word2vec_format("sentiment/embedding/eng_emb/wiki-news-300d-1M.vec")
    for word in vocab_set:
        if word in word2vec_model:
            vocab2vec[word] = word2vec_model[word]
        else:
            vocab2vec[word] = np.zeros(word2vec_model.vector_size)
    print("We have {} word vectors".format(len(vocab2vec)))
    vector_size = word2vec_model.vector_size
    return vocab2vec, vector_size

def get_embedding_matrix(tokenizer, word2vec_model, vector_size, num_words = 7000):
    vocab = tokenizer.index_word
    embedded_matrix = np.zeros((num_words, vector_size))
    vocab = tokenizer.index_word
    word_exits = 0
    for word in vocab:
        if word > num_words:
            break
        try:
            embedded_matrix[word-1, :] = word2vec_model[vocab[word]]
            word_exits += 1
        except KeyError:
            if word == 0:
                embedded_matrix[word, :] = np.zeros(word2vec_model.vector_size)
            else:
                # 0.25 is embedding SD
                embedded_matrix[word, :] = np.random.uniform(-0.25, 0.25, 300)
    print("Found %s word in embedding file" %word_exits)
    return embedded_matrix