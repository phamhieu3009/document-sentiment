from sentiment.models.classifier.HARNN import HARNN_model
import argparse
from gensim.models import keyedvectors
from sentiment.embedding import get_embedding

parser = argparse.ArgumentParser()
parser.add_argument("--test_cases", type = str, default = "test/test.txt", help = "list of text to predict sentiment")
parser.add_argument("--lang", default = "viet", help = "language of input data")
parser.add_argument("--load_dir", type = str, default = "HARNN/models/HAN.ckpt", help = "path to load model file")

opt = parser.parse_args()
print(opt)

print(f"Load model from path: {opt.load_dir}")

with open(opt.test_cases) as f:
  text_list = f.readlines()
for i in range(len(text_list)):
  text_list[i] = text_list[i].replace("\n", "")

vocab2vec, vector_size = get_embedding(text_list, lang = opt.lang, processing = True)


model = HARNN_model(vocab2vec, vector_size, num_class = 2)

result = model.predict(text_list, model_path = opt.load_dir)

