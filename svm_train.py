from __future__ import print_function
from sklearn import metrics
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.model_selection import cross_val_score
from sklearn.svm import LinearSVC
import pandas as pd
import re
import string
import codecs
import pickle


import argparse
import pandas as pd

from sentiment.models.classifier.SVM_TFIDF import sentiment_dict
from sentiment.models.classifier.SVM_TFIDF import SVM_TFIDF

parser = argparse.ArgumentParser()
parser.add_argument('--trainroot', required=True, help='path to training dataset')
parser.add_argument("--valroot", type = str, default = None, help = "path to validation dataset")
parser.add_argument("--lang", default = "viet", help = "language of training data")
parser.add_argument('--train_test_split', type = int, default = 0.2,  help = "validation data size, activated if valroot is None")
parser.add_argument("--save_dir", type = str, default = "HARNN/models", help = "path to save model file")

opt = parser.parse_args()
print(opt)

train = pd.read_csv(opt.trainroot)

X_train = train.review.values
y_train = train.label.values

if opt.valroot is None:
  X_train, X_test, y_train, y_test = train_test_split(X_train, y_train, test_size = opt.train_test_split)
else:
  val = pd.read_csv(opt.valroot)
  X_test = val.review.values
  y_test = val.label.values

model = SVM_TFIDF()
model.train(X_train, y_train, X_test, y_test, save_dir = opt.save_dir)
